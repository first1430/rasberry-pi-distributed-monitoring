import socket
import threading
import SocketServer
import time
import os,sys
import json 
import globals
globals.init()
from datetime import datetime

from pprint import pprint as dump_val


#My Files 
import config

sys.path.insert(0, './lib')
from tcp import client as send
import datahandler
import messagegen

from measure import *

import udpserv

### Global Var for server sharing with looping program


################################

class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
	def handle(self):
		data = self.request.recv(65536).strip()
		print "Recieved %s"%data
		print "len %d"%len(data)
		datahandler.DataHandler(data)
		cur_thread = threading.current_thread()
		response = "%s: %s"%(cur_thread.name, data) ## Data that will response to client
		self.request.sendall(response)

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
	pass
 
class FuncThread(threading.Thread):
	def __init__(self, target, *args):
		self._target = target
		self._args = args
		threading.Thread.__init__(self)
 
	def run(self):
		self._target(*self._args)
 


def CheckRootPermission():
	if os.geteuid() != 0:
		sys.exit('Sorry, This Program need root permission.')

def Thread_Func(funct,args):
	thread_prog = threading.Thread(target=funct, args=args)
	thread_prog.start()
	return thread_prog

if __name__ == "__main__":

	#checkrunning as root
	CheckRootPermission()

	# Port 0 means to select an arbitrary unused port
	host, port = "localhost", 0	

	server = ThreadedTCPServer((host, port), ThreadedTCPRequestHandler)
	ip, port = server.server_address
	# Start a thread with the server -- that thread will then start one
	# more thread for each request
	server_thread = threading.Thread(target=server.serve_forever)

	port = server.server_address[1]
	config.DEV_PORT = server.server_address[1]
	
	#dump_val(vars(server_thread)) #For dumping thread value

	# Exit the server thread when the main thread terminates
	server_thread.daemon = True
	server_thread.start()

	#TestGIT
	print "SEND DEVICE START TO CENTRAL"
	message = messagegen.StartnewdeviceMessage(config.DEV_NAME,config.DEV_IP+":"+str(port),config.DEV_GROUP,config.DEV_PORT)
	res = send(config.SERVER_IP,config.SERVER_PORT , message)


	print "Client start running in %s PORT %d"%(server_thread.name,port)
	i = 0
	markping = 0
	markstandardref = 0
	markpacketloss = 0

	#Start Server For Packet loss
	sv_thread = Thread_Func(funct = udpserv.UDP_SERVER, args = [])
	
	while True:
		try:
			print datetime.now().strftime('%Y/%m/%d %H:%M:%S')
			time.sleep(1)
			i += 1
			
			if int(time.time()) % 10 == 0: 
				#Send keepalive every ten sec
				#message = messagegen.SendKeepAlive(config.DEV_NAME,config.DEV_IP+":"+str(port),config.DEV_GROUP, config.DEV_PORT)
				#res = send(config.SERVER_IP,config.SERVER_PORT , message)
				pass

			if int(time.time()) % 60 == 0 and markpacketloss != int(time.time())/45: 
				markpacketloss = int(time.time())/45
				packetloss_thread = Thread_Func(funct = CheckPacketLoss, args = ["127.0.0.1",sv_thread])
				pass
			if int(time.time()) % 30 == 0 and markstandardref != int(time.time())/30: 
				markstandardref = int(time.time())/30
				standardref_thread = Thread_Func(funct = CheckStandardref, args = [])

			if int(time.time()) % 5 == 0 and markping != int(time.time())/5: 
				markping = int(time.time())/5
				ping_thread = Thread_Func(funct = CheckPing, args = ["ku.ac.th"])
			
			if globals.shared_mem: #Check Shared_MEM is empty? if not fetch and send result
				while globals.shared_mem:
					key,val = globals.shared_mem.popitem()
					report_data = val
					message = messagegen.SendTestData(NAME=config.DEV_NAME,
							IP=config.DEV_IP,
							GROUP=config.DEV_GROUP, 
							PORT=config.DEV_PORT,
							TYPE=key.lower(),
							IPTGT="192.168.1.1",
							DATA=report_data)
					send(config.SERVER_IP,config.SERVER_PORT , message)
			
		except (KeyboardInterrupt,SystemExit):
			server.shutdown()
			print "\nExiting All Process"
			for thread in threading.enumerate():
				if thread.isAlive():
					try:
						thread._Thread__stop()
					except:
						print(str(thread.getName()) + ' could not be terminated')

			sys.exit("Done Bye ~*")

