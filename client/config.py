import sys,socket

sys.path.insert(0, './lib')
import getprofile

DEV_NAME = getprofile.hostname()  #"Raspi1"
DEV_IP = getprofile.ip_address("wlan0")  #"158.108.40.31"
DEV_GROUP = "ENG"
DEV_PORT = 0

#ONLY USED FOR FIRST REGISTRATION
FACULTY = "Engineering"
BUILDING = "13"
FLOOR = "7"
ROOMNUMBER="703"
REGISTRATION_PERSON = "Test"
EMAIL = "Firstrap7@gmail.com"
TEL = "085xxxxxxx"

SERVER_IP = "127.0.0.1" 
SERVER_PORT = 9001 #PORT THAT SEND DATA TO
