import socket
import struct
import uuid
try:
	import fcntl
	linux = 1
except: #It's linux Lib
	linux = 0


	
def ip_address(ifname):
	if (linux == 1):
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		return socket.inet_ntoa(fcntl.ioctl(s.fileno(),0x8915,struct.pack('256s', ifname[:15]))[20:24])
	else:
		return socket.gethostbyname(socket.gethostname())

def mac(ifname):
	if linux == 1:
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', ifname[:15]))
		return ':'.join(['%02x' % ord(char) for char in info[18:24]])
	else:
		return uuid.getnode()

def hostname():
	return socket.gethostname()


