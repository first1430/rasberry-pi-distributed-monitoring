import json
import config
def StartnewdeviceMessage(NAME,IP,GROUP,PORT):
    Cont = {"IP":IP,"NAME":NAME,"GROUP":GROUP,"PORT":PORT}
    Message = json.dumps({"MES_TYPE":"STARTNEWDEVICE","FROM":Cont})
    return Message

def SendKeepAlive(NAME,IP,GROUP,PORT):
	Cont = {"IP":IP,"NAME":NAME,"GROUP":GROUP,"PORT":PORT,"STATUS":"ONLINE"}
	Message = json.dumps({"MES_TYPE":"KEEPALIVE","FROM":Cont})
	return Message

def SendTestData(NAME,IP,GROUP,PORT,TYPE,IPTGT,DATA):
	Fr = {"IP":IP,"NAME":NAME,"GROUP":GROUP,"PORT":PORT}
	if TYPE == "pingdelay" or TYPE == "packetloss" or TYPE == "linkavailability":
		#These test have a pair
		Cont = {"PAIR": IP+"-"+IPTGT,
				"DATA": str(DATA)}
	elif TYPE == "standardref" or TYPE == "httpspeed":
		Cont = {"DATA": str(DATA)}
		#These test do not have a pair
	else:
		#wrong type
		return None
	Message = json.dumps({"MES_TYPE":"SENDDATA","FROM":Fr,"CONTENT":Cont,"TYPE":TYPE})
	return Message