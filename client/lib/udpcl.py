from socket import *
import string
import time
import json

def UDP_CLIENT(dest="127.0.0.1",totaltestsize=10**5,result_timeout=10):
	port = 8765
	host = "0.0.0.0"
	port_recieve = 8567
	buffer = 102400


	UDPSock = socket(AF_INET,SOCK_DGRAM)
	UDPSock.setsockopt(SOL_SOCKET,SO_REUSEADDR,1)
	UDPSock.bind((host,port_recieve))

	#print "-" * 40
	#print "UDP-CLIENT"
	#print "-" * 40
	#print "sending to %s port %s" % (dest, port)

	datasize = 1

	for i in reversed(range(2,65001)): #SELECT SIZE TO SEND
		#print i
		if totaltestsize % i == 0:
			datasize = i
			break

	numtimes = totaltestsize/datasize

	data = "x"*datasize

	#print "Datasize = %d , Sent %d times"%(datasize,numtimes)

	while True:
		try:
			for x in range(numtimes):
				if(UDPSock.sendto(data,(dest,port))):
					pass
				else:
					#failed to sent this time make it slower to sent
					time.sleep(0.0001)

			#to tell server that dones
			UDPSock.sendto("%dbytes"%(totaltestsize), (dest,port))
			#print "Done."
			break
		except:
			#print "Send failed"
			time.sleep(10)


	result = [0,0,0,100] #Default all packet lost value or not found server

	ResultSock = socket(AF_INET, SOCK_STREAM)
	ResultSock.setsockopt(SOL_SOCKET,SO_REUSEADDR,1)
	ResultSock.bind((host, port_recieve))
	ResultSock.listen(1)
	ResultSock.settimeout(result_timeout)

	while True:
		try:
			#print "waiting for Result"
			connection, client_address = ResultSock.accept()
			#print 'client connected:', client_address
			if client_address[0] != dest:
				#print "REJECT %s "%client_address[0]
				break

			alldata = ""
			while True:
				data = connection.recv(1024)
				#print 'received :%s ' % data
				if data:
					alldata += data
				if "end" in alldata:
					break
			
			connection.sendall("ack")
			connection.close()
			result = data.split("end")[0]
			break
			connection.close()
		except:
			#print "Timeout"
			break
	ResultSock.close()


	return json.loads(result)

#print UDP_CLIENT(dest="127.0.0.1",totaltestsize=10**5)