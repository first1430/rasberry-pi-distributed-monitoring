#/usr/bin/env python
import httplib,urllib
import random
import socket
import sys
import getpass
import bz2
import commands

if len(sys.argv) < 2:
	print 'Usage: '+sys.argv[0]+' [option]'
	print '      -a      Set Username and Password'
	print '      -l      login'
	sys.exit()
def readFile(name):
	text=''
	f=open(name, 'r')
	text=f.readline()
	f.close()
	return bz2.decompress(text)
def writeFile(name,text):
	f=open(name,'w')
	f.write(text)
	f.close()
if(sys.argv[1]=='-a'):
	username = raw_input("Username: ")
	passwd = getpass.getpass("Password: ")
	cpasswd = getpass.getpass("Confirm Password: ")
	if(passwd==cpasswd and passwd!=''):
		writeFile('.user.bin',bz2.compress(username))
		writeFile('.pass.bin',bz2.compress(passwd))
	sys.exit()

if sys.argv[1]=='-l':
	username=''
	password=''
	try:
		username=readFile('.user.bin')
		password=readFile('.pass.bin')
	except:
		print "The configuration is not initialized! Please use the -a option"
		sys.exit();
	ipAddr=''
	try:
		ipAddr=socket.gethostbyname(socket.gethostname())
	except:
		ipAddr= commands.getoutput("/sbin/ifconfig").split("\n")[1].split()[1][5:]
	print "login ip:" + ipAddr
	url="login"+str(random.randint(1,12))+".ku.ac.th"
	params=urllib.urlencode({'username': username, 
						'password': password, 
						'zone': '0',
						'ssid':'-',
						'bssid':'-',
						'speed':'1Gbps',
						'v':'4'
						,'targetip':ipAddr
						})
	headers = {"Content-Type": "application/x-www-form-urlencoded",'Content-Length':str(len(params))}
	conn = httplib.HTTPSConnection(url,443)
	conn.request("POST", '/mobile.php?action=login',params, headers)
	resp = conn.getresponse()
	print resp.read()
	conn.close()