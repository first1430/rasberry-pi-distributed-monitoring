import socket
def client(ip, port, message):
    print "SEND %s"%message
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
    	sock.connect((ip, port))
    except:
    	return "ERR:SEND_FAILED"
    try:
        sock.sendall(message)
        response = sock.recv(65536)
        return response
    finally:
    	sock.close()