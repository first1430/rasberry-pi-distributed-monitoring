from socket import *
import time

def UDP_SERVER():
	host = "0.0.0.0"
	totaltestsize = 10**7 #total byte recieved from client
	port = 8765

	port_sendresult = 8567
	timeout = 50 #timeout in second


	buffer = 102400


	UDPSock = socket(AF_INET, SOCK_DGRAM)
	UDPSock.setsockopt(SOL_SOCKET,SO_REUSEADDR,1)
	UDPSock.bind((host,port))


	totalbytes = 0
	startstamp = 10**10
	timestamp = -1

	#print "-" * 40
	#print "UDP-SERVER"
	#print "-" * 40
	#print "running on %s port %s" % (host, port)


	totalrcvs = 0
	flag = 0
	flagip = ""

	while 1:
		data,addr = UDPSock.recvfrom(buffer)

		if not data:
			#print "No data."
			break
		else:
			if flagip != addr[0] and flagip != "":
				#Prevent UDP packet from unknown source
				continue

			if flag == 0:
				startstamp = time.time()
				flag = 1
				flagip = addr[0]

			if "bytes" in data and data.split("bytes")[0].isdigit():
				donestamp = time.time()
				totaltestsize = int(data.split("bytes")[0])
				totaltime = (time.time() - startstamp)
				avg_speed = totalbytes/totaltime/1000
				packetloss_percent = ((totaltestsize-totalbytes)*100.0/totaltestsize)
				#print "Total %d bytes in %.3f"%(totalbytes,totaltime)
				#print "Avg Speed %.1f kbps"%(avg_speed)
				#print "Packet loss %.2f%%"%(packetloss_percent)
				#print ""


				sock = socket(AF_INET, SOCK_STREAM)

				try:
					# Connect to server and send data
					sock.connect((flagip,port_sendresult))
					res = "[%s,%s,%s,%s]end"%(totalbytes,totaltime,avg_speed,packetloss_percent)
					sock.sendall(res)

					# Receive data from the server and shut down
					received = sock.recv(1024)
				except:
					#print "Can't Connect With Server"
					pass

				sock.close()

				#UDPSock.sendto("[%s,%s,%s,%s]"%(totalbytes,totaltime,avg_speed,packetloss_percent),(flagip,port_sendresult))
				flag = 0
				flagip = ""
				totalbytes = 0
				totalrcvs = 0
				continue
				#break
			
			data = len(data)
			


			totalbytes += data
			totalrcvs += 1

			#rate = totalbytes/(donestamp - timestamp + 0.0001) * 8 / 1000

			#print "\nRcvd: %s bytes, %s total in %.6f s at %s kbps" % (data, totalbytes, (donestamp - timestamp), rate)
			timestamp = time.time()
			#print timestamp
	UDPSock.close()


#UDP_SERVER()
