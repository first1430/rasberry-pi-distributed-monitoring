import json
import threading

#My Lib
import globals
global dev_list
from measure import *


def Thread_Func(funct,args):
	thread_prog = threading.Thread(target=funct, args=args)
	thread_prog.start()
	return thread_prog

def ErrorHandler(Type):
	print Type
	pass

def DataHandler(Data):
	try:
		print Data
		Proc_Data = json.loads(Data)

		if Proc_Data["MES_TYPE"] == "TESTPINGv4":
			Content = Proc_Data["CONTENT"]
			From = Content["From"]
			Target = Content["Target"]
			print "Ping %s"%(Target)
			ping_thread = Thread_Func(funct = CheckPing, args = [From])

	except:
		ErrorHandler("Got Incorrect Format Data")