import subprocess
import urllib,httplib
import random
from base64 import b64decode as decode
from time import sleep
import subprocess, datetime, os, time, signal

import ssl

DHCPSTEP = 0
DNSSTEP = 0
LOGINSTEP = 0

def timeout_command(command, timeout,stderr=False):
	start = datetime.datetime.now()
	process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	while process.poll() is None:
	  time.sleep(0.2)
	  now = datetime.datetime.now()
	  if (now - start).seconds> timeout:
		os.kill(process.pid, signal.SIGKILL)
		os.waitpid(-1, os.WNOHANG)
		return None
	if stderr == True:
		return process.stderr.read()
	return process.stdout.read()

def TestDHCP():
	try:
		checkDHCP = timeout_command(["dhclient","wlan0","-4","-v"],timeout=20,stderr=True)
		if checkDHCP == None:
			return 0
		if "DHCPACK" in checkDHCP:
			return 1
	except subprocess.CalledProcessError:
		pass
		#print "DHCP FAILED"
	return 0


#sleep(1)
def TestDNS(domain="login1.ku.ac.th"):
	try:
		#dnslookup = subprocess.check_output("nslookup "+domain,shell=True)
		dnslookup = timeout_command(["nslookup",domain],timeout=10)
		if dnslookup == None:
			##print "Can't find dns"
			return 0
		spli =  dnslookup.split()
		for i in xrange(len(spli)-1):
			if spli[i] == "Name:" and spli[i+1] == domain: 
				##print "FOUND"
				return 1
		
		#print "NOT FOUND"
		return 0
	except subprocess.CalledProcessError:
		##print "DNS LOOKUP FAILED OR NOT FOUND"
		return 0
	return 0

def TestKULogin(username="b5410501501",password=decode("Zmlyc3RyYXB5bzE=")):
	ipAddr= subprocess.check_output("/sbin/ifconfig").split("\n")[1].split()[1][5:]
	params=urllib.urlencode({'username': username, 
						'password': password, 
						'zone': '0',
						'ssid':'-',
						'bssid':'-',
						'speed':'1Gbps',
						'v':'4'
						,'targetip':ipAddr
						})
	headers = {"Content-Type": "application/x-www-form-urlencoded",'Content-Length':str(len(params))}
	for i in xrange(1):
		try:
			##print "Round %d"%i
			url="login"+str(i)+".ku.ac.th"
			conn = httplib.HTTPSConnection(url,port=443,timeout=10)
			conn.request("POST", '/mobile.php?action=login',params, headers)
			resp = conn.getresponse()
			if resp.status == 200:
				data = resp.read()
				if data == "OK":
					return 1
				else:
					return 0
		except:
			pass
	return 0

def TestAll():
	try:
		Result = []
		Result.append(1)
		#Reserved for Checking Physical connection of line

		#print "#"*30
		#print "STEP 1: Test DHCP"
		#print "Testing..."
		Result.append(TestDHCP())
		#Result.append(1)
		#print "done"
		#print "#"*30
		#print "STEP 2: Test DNS"
		#print "Testing..."
		Result.append(TestDNS("login.ku.ac.th"))
		#print "done"
		#print "#"*30
		#print "STEP 3: Test Login KUWIN"
		#print "Testing..."
		Result.append(TestKULogin())
		#print "done" 
		return Result
	except:
		return [0,0,0,0]
	return [0,0,0,0]