from socket import *
import string
import time


dest = "127.0.0.1"
port = 8105

host = "0.0.0.0"
port_recieve = 8104
totaltestsize = 10**5

buffer = 102400


UDPSock = socket(AF_INET,SOCK_DGRAM)
UDPSock.bind((host,port_recieve))
print "-" * 40
print "UDP-CLIENT"
print "-" * 40
print "sending to %s port %s" % (dest, port)

datasize = 1

for i in reversed(range(2,65001)): #SELECT SIZE TO SEND
	#print i
	if totaltestsize % i == 0:
		datasize = i
		break

numtimes = totaltestsize/datasize

data = "x"*datasize

print "Datasize = %d , Sent %d times"%(datasize,numtimes)

while (1):
	try:
		for x in range(numtimes):
			if(UDPSock.sendto(data,(dest,port))):
				pass
			else:
				#failed to sent this time make it slower to sent
				time.sleep(0.0001)

		#to tell server that dones
		UDPSock.sendto("%dbytes"%(totaltestsize), (dest,port))
		print "Done."
		break
	except:
		print "Send failed"
		time.sleep(10)

while(1):
	data,addr = UDPSock.recvfrom(buffer)
	if data:
		print data
		break
UDPSock.close()
