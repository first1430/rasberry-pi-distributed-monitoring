#Library that used for measuring Network 
from ping import *
import dhcp
import udpcl,udpserv
import globals
global shared_mem

from datetime import datetime

def CheckPing(url):
	nowtime = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
	Result = Ping().GetAllDelay(url,timeout=20)
	if Result == "FAILPING":
		Result = [20000,20000,20000]
	print "Test Ping Done"
	#Keep to global buffer
	globals.shared_mem["PINGDELAY"] = [nowtime,Result[0],Result[1],Result[2]]


def CheckStandardref():
	nowtime = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
	Result = dhcp.TestAll()
	print "STANDARD REF DONE CHECK"
	globals.shared_mem["STANDARDREF"] = [nowtime,Result[0],Result[1],Result[2],Result[3]]


def CheckPacketLoss(dest,svthread):
	try:
		nowtime = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
		
		if svthread.isAlive() == False:
			print "Couldn't find server"
			return None

		Packetsize = 10**5
		Result = udpcl.UDP_CLIENT(dest="127.0.0.1",totaltestsize=10**5)
		print "TEST PACKETLOSS DONE"
		#(totalbytes,totaltime,avg_speed,packetloss_percent)
		globals.shared_mem["PACKETLOSS"] = [nowtime,Result[0],Result[1],Result[2],Result[3]]
	except Exception,e :
		writefile("Caught: "+str(e))



def writefile(string):
	fi = open("./log/err.log","a")
	fi.write(string+"\n")
	fi.flush()
	fi.close()

