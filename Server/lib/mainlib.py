import socket

import struct
try:
	import fcntl
	linux = 1
except: #It's linux Lib
	linux = 0


	
def get_ip_address(ifname):
	if (linux == 1):
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		return socket.inet_ntoa(fcntl.ioctl(
			s.fileno(),
			0x8915,  # SIOCGIFADDR
			struct.pack('256s', ifname[:15])
		)[20:24])
	else:
		return socket.gethostbyname(socket.gethostname())
