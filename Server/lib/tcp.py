import socket
import datahandler
def client(ip, port, message):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
    	sock.connect((ip, port))
    except:
    	return "ERR:SEND_FAILED"
    try:
        sock.sendall(message)
        response = sock.recv(65536)
        print "ACK : %s"%format(response)
        return response
    except:
        datahandler.ErrorHandler("Error Connection to %s:%s Reset by PEER"%(ip,port))
    sock.close()

            
