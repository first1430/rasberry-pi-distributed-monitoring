import json
import time
import os
from datetime import datetime

### Self implementation file
import globals
global dev_list
import config

def ErrorHandler(Type):
	print Type
	pass

def DataHandler(Data):
	#try:
		print Data
		Proc_Data = json.loads(Data)
		if Proc_Data["MES_TYPE"] == "STARTNEWDEVICE":
			ReceiveAddNewDevice(From=Proc_Data["FROM"])
			
		if Proc_Data["MES_TYPE"] == "KEEPALIVE":
			ReceiveKeepAlive(From=Proc_Data["FROM"])

		if Proc_Data["MES_TYPE"] == "SENDDATA":
			ReceiveTestData(From=Proc_Data["FROM"],Type=Proc_Data["TYPE"],Content=Proc_Data["CONTENT"])
	#except:
		#ErrorHandler("Got Incorrect Format Data")


def ReceiveAddNewDevice(From):
	grp = From["GROUP"]
	name = From["NAME"]
	ip = From["IP"]
	port = From["PORT"]
	if not globals.dev_list.has_key(grp): #CHECK THAT GROUP DOESN'T EXISTS
		globals.dev_list[grp] = {}

	if not globals.dev_list[grp].has_key(ip):
		globals.dev_list[grp][ip] = {}
		globals.dev_list[grp][ip]["NAME"] = name
		globals.dev_list[grp][ip]["LAST_TIMESTAMP"] = time.time()
		globals.dev_list[grp][ip]["PORT"] = port
	return True

def ReceiveKeepAlive(From):
	#Update Device Keepalive
	#print "KEEPALIVE IS COMING"
	grp = From["GROUP"]
	name = From["NAME"]
	ip = From["IP"]
	stat = From["STATUS"]
	port = From["PORT"]
	if stat == "ONLINE":
		globals.dev_list[grp][ip]["LAST_TIMESTAMP"] = time.time()
		globals.dev_list[grp][ip]["PORT"] = port
		return True
	return False

def ReceiveTestData(From,Type,Content):
	print "TESTDATA"
	grp = From["GROUP"]
	name = From["NAME"]
	ip = From["IP"]
	port = From["PORT"]

	print "Receive %s"%Type
	datwrite = "\t".join(map(str,eval(Content["DATA"])))
	fname = datetime.now().strftime('%Y-%m-%d.%H')
	if Content.has_key("PAIR"):
		pair = Content["PAIR"]+"/"
	else:
		pair = ""
	directory = config.REPORT_PATH+"report/"+Type+"/v4/"+pair
	if not os.path.exists(directory):
		os.makedirs(directory)
	f = open(directory+fname+".log","a")
	f.write(datwrite+"\n")
	f.flush()
	f.close()


	pass


#"\n".join(["\t".join(map(str,x)) for x in a])