import socket
import threading
import SocketServer
from datetime import datetime
import time

from pprint import pprint as dump_val
from tcp import client



### Global Var for server sharing with looping program

###
class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        data = self.request.recv(1024).strip()
        print "Recieved %s"%data
        print len(data)
        cur_thread = threading.current_thread()
        response = "%s: %s"%(cur_thread.name, data) ## Data that will response to client
        self.request.sendall(response)

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

if __name__ == "__main__":
    # Port 0 means to select an arbitrary unused port
    HOST, PORT = "localhost", 5000

    server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler)
    ip, port = server.server_address
    # Start a thread with the server -- that thread will then start one
    # more thread for each request
    server_thread = threading.Thread(target=server.serve_forever)



    # Exit the server thread when the main thread terminates
    server_thread.daemon = True
    server_thread.start()
    print "Server start running in %s"%server_thread.name
    i = 0
    while i < 30:
        print datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        time.sleep(1)
        i += 1
    server.shutdown()