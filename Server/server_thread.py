import socket
import threading
import SocketServer
import time
import sys
import json
import globals
globals.init()
from datetime import datetime

from pprint import pprint as dump_val


#My Files 
import config
sys.path.insert(0, './lib')
from tcp import client as send
import datahandler
import messagegen

print config.DEV_NAME



class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
	def handle(self):
		data = self.request.recv(65536).strip()
		print "Recieved %s"%data
		print "len : %d"%len(data)
		datahandler.DataHandler(data)
		cur_thread = threading.current_thread()
		response = "done" ## Data that will response to client
		self.request.sendall(response)

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
	pass

if __name__ == "__main__":
	# Port 0 means to select an arbitrary unused port
	HOST, PORT = "localhost", config.SERVER_PORT	

	server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler)
	ip, port = server.server_address
	# Start a thread with the server -- that thread will then start one
	# more thread for each request
	server_thread = threading.Thread(target=server.serve_forever)



	# Exit the server thread when the main thread terminates
	server_thread.daemon = True
	server_thread.start()
	print "Server start running in %s PORT %d"%(server_thread.name,config.SERVER_PORT)
	i = 0
	while True:

		try:
			
			print "\n"+datetime.now().strftime('%Y/%m/%d %H:%M:%S') + " " + str(globals.dev_list)			
			
			'''
			if int(time.time() % 5) == 0:
				print "SEND TEST Ping"
				print messagegen.SENDTESTPING("127.0.0.1","192.168.1.1")
				# # USED TO SEND SOMETHING
				gg = globals.dev_list
				iptosend = gg["ENG"].keys()[0].split(":")[0]
				port = gg["ENG"].values()[0]["PORT"]
				print (iptosend,port)
				send("127.0.0.1",int(port),messagegen.SENDTESTPING("127.0.0.1","192.168.1.1"))
			'''
			time.sleep(1)
		except (KeyboardInterrupt,SystemExit):
			server.shutdown()
			print "\nExiting All Process"
			for thread in threading.enumerate():
				if thread.isAlive():
					try:
						thread._Thread__stop()
					except:
						print(str(thread.getName()) + ' could not be terminated')

			sys.exit("Done Bye ~*")
	server.shutdown()
